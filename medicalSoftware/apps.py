from django.apps import AppConfig


class MedicalsoftwareConfig(AppConfig):
    name = 'medicalSoftware'
