from django.urls import path
from .views import MedicalSoftwareView, RegisterPatientView, BookAppointmentView

urlpatterns = [
	path('appointment/', BookAppointmentView.as_view(), name='book_appointment'), # new
	path('patient/', RegisterPatientView.as_view(), name='register_patient'), # new
	path('', MedicalSoftwareView.as_view(), name='home'),
]