from django.views.generic import TemplateView, ListView, DetailView
#from django.views.generic.edit import RegisterPatientView, BookAppointmentView
from django.urls import reverse_lazy # new

#from .models import Post

# Create your views here.
class MedicalSoftwareView(TemplateView):
	template_name = 'home.html'
	
class RegisterPatientView(TemplateView): # new
	template_name = 'register_patient.html'
	
class BookAppointmentView(TemplateView): # new
	template_name = 'book_appointment.html'
	
	
