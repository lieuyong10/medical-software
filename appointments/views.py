from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.urls import reverse_lazy

from .forms import AppointmentForm
from .models import Appointment

from datetime import datetime
from pytz import timezone, utc
from django.conf import settings

from django.contrib.auth import get_user_model

from django.http import JsonResponse
import pdb;

# Create your views here.

class AppointmentListView(ListView):
	model = Appointment
	template_name = 'appointment_list.html'
	# This is our new breakpoint
	#pdb.set_trace()
	
class AppointmentDetailView(DetailView):
	model = Appointment
	template_name = 'appointment_detail.html'

class AppointmentUpdateView(UpdateView):
	model = Appointment
	form_class = AppointmentForm
	#fields = ('patient', 'purpose', 'startDate', 'startTime', 'endDate', 'endTime', 'doctor')
	template_name = 'appointment_edit.html'
	
class AppointmentDeleteView(DeleteView):
	model = Appointment
	template_name = 'appointment_delete.html'
	success_url = reverse_lazy('appointment_list')

class AppointmentCreateView(CreateView):
	model = Appointment
	#form_class = AppointmentForm
	fields = ('patient', 'purpose', 'startDate', 'startTime', 'endDate', 'endTime', 'doctor')
	template_name = 'appointment_new.html'
	

def appointment_calendar(request):		
	schedules = []	
	scheduleRow = {}	
	doctorIDs = []
	doctors = []
	doctorRow = {}
		
	queryset = Appointment.objects.values()
	
	
	for row in queryset:
		#scheduleRow = {'id': row['id'], 'calendarId': row['patient_id'], 'title': row['purpose'], 'category': 'time', 'dueDateClass': '', 'start': row['startDate'].strftime('%Y-%m-%d') + 'T' + row['startTime'].strftime('%I:%M:%S') + '+08:00', 'end': row['endDate'].strftime('%Y-%m-%d') + 'T' + row['endTime'].strftime('%I:%M') + '+08:00', 'bgColor': '#37EAE1', 'attendees': [getattr(get_user_model().objects.get(id=row['doctor_id']), 'first_name')]}
		scheduleRow = {'id': row['id'], 'calendarId': row['patient_id'], 'title': row['purpose'], 'body': row['purpose'], 'category': 'time', 'dueDateClass': '', 'start': utcisoformat(datetime(row['startDate'].year, row['startDate'].month, row['startDate'].day, row['startTime'].hour, row['startTime'].minute, row['startTime'].second)), 'end': utcisoformat(datetime(row['endDate'].year, row['endDate'].month, row['endDate'].day, row['endTime'].hour, row['endTime'].minute, row['endTime'].second)), 'bgColor': '#37EAE1', 'attendees': [getattr(get_user_model().objects.get(id=row['doctor_id']), 'first_name')]}
		schedules.append(scheduleRow)
		
		if row['doctor_id'] not in doctorIDs:
			doctorIDs.append(row['doctor_id']);
		

	for doctorID in doctorIDs:
		doctorRow = {
			'doctor_id': doctorID, 
			'first_name': getattr(get_user_model().objects.get(id=doctorID), 'first_name'), 
			'last_name': getattr(get_user_model().objects.get(id=doctorID), 'last_name')
		}
		doctors.append(doctorRow)
		
	#pdb.set_trace()
	
	
	return JsonResponse(data={
		'schedules': schedules,
		'doctors': doctors
	})
	
def select_doctor(request):
	schedules = []	
	scheduleRow = {}	
	doctorIDs = []
	doctors = []
	doctorRow = {}
	
	# get filter
	if (request.method=='GET'):
		filter_doctor_id = request.GET['doctor_id']
	
	queryset = Appointment.objects.filter(doctor_id=filter_doctor_id).values()
	
	#pdb.set_trace()
	
	for row in queryset:
		scheduleRow = {'id': row['id'], 'calendarId': row['patient_id'], 'title': row['purpose'], 'body': row['purpose'], 'category': 'time', 'dueDateClass': '', 'start': utcisoformat(datetime(row['startDate'].year, row['startDate'].month, row['startDate'].day, row['startTime'].hour, row['startTime'].minute, row['startTime'].second)), 'end': utcisoformat(datetime(row['endDate'].year, row['endDate'].month, row['endDate'].day, row['endTime'].hour, row['endTime'].minute, row['endTime'].second)), 'bgColor': '#37EAE1', 'attendees': [getattr(get_user_model().objects.get(id=row['doctor_id']), 'first_name')]}
		schedules.append(scheduleRow)
		
		if row['doctor_id'] not in doctorIDs:
			doctorIDs.append(row['doctor_id']);	
		
	for doctorID in doctorIDs:
		doctorRow = {
			'doctor_id': doctorID, 
			'first_name': getattr(get_user_model().objects.get(id=doctorID), 'first_name'), 
			'last_name': getattr(get_user_model().objects.get(id=doctorID), 'last_name')
		}		
		doctors.append(doctorRow)
	
	return JsonResponse(data={
		'schedules': schedules
	})

def utcisoformat(dt):
    """
    Return a datetime object in ISO 8601 format in UTC, without microseconds
    or time zone offset other than 'Z', e.g. '2011-06-28T00:00:00Z'. Useful
    for making Django DateTimeField values compatible with the
    jquery.localtime plugin.
    """
    # Convert datetime to UTC, remove microseconds, remove timezone, convert to string
    return timezone(settings.TIME_ZONE).localize(dt.replace(microsecond=0)).astimezone(utc).replace(tzinfo=None).isoformat() + 'Z'
	
# def update_appointment(request):
	# if 'doctor_id' in request.POST:
		# Appointment.objects.filter(id=request.POST['doctor_id']).update(startDate=