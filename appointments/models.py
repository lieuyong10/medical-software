from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse
from patients.models import Patient
from datetime import datetime

# Create your models here.

class Appointment(models.Model):
	patient = models.ForeignKey(
		Patient,
		on_delete=models.CASCADE,
	)
	purpose = models.CharField(max_length=255)
	startDate = models.DateField(default=datetime.now(), blank=True)
	startTime = models.TimeField(blank=True)
	endDate = models.DateField(default=datetime.now(), blank=True)
	endTime = models.TimeField(blank=True)
	doctor = models.ForeignKey(
		get_user_model(),
		on_delete=models.CASCADE,
	)
	
	def __str__(self):
		return str(self.patient) + ' ' + str(self.purpose) + ' on ' + str(self.startDate) + ' at ' + str(self.startTime)
	
	def get_absolute_url(self):	
		return reverse('appointment_detail', args=[str(self.id)])