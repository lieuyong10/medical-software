# users/forms.py

from django import forms
from .models import Appointment

class AppointmentForm(forms.ModelForm):
	
	class Meta:
		model = Appointment
		fields = ('patient', 'purpose', 'startDate', 'startTime', 'endDate', 'endTime', 'doctor')
		startTime = input_formats=['%I:%M %p', ]
		endTime = input_formats=['%I:%M %p', ]
	
		