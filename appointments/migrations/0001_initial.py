# Generated by Django 2.1.5 on 2020-05-25 03:41

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('patients', '0008_auto_20200505_1659'),
    ]

    operations = [
        migrations.CreateModel(
            name='Appointment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('patientCalendarId', models.CharField(max_length=255)),
                ('title', models.CharField(max_length=255)),
                ('category', models.CharField(max_length=255)),
                ('dueDateClass', models.CharField(max_length=255)),
                ('start', models.CharField(max_length=255)),
                ('end', models.CharField(max_length=255)),
                ('doctor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('patient', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='patients.Patient')),
            ],
        ),
    ]
