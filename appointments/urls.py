from django.urls import path

from .views import (
	AppointmentListView,
	AppointmentUpdateView,
	AppointmentDetailView,
	AppointmentDeleteView,
	AppointmentCreateView,
	appointment_calendar,
	select_doctor,
)

urlpatterns = [
	path('<int:pk>/edit/', AppointmentUpdateView.as_view(), name='appointment_edit'),
	path('<int:pk>/', AppointmentDetailView.as_view(), name='appointment_detail'),
	path('<int:pk>/delete/', AppointmentDeleteView.as_view(), name='appointment_delete'),
	path('new/', AppointmentCreateView.as_view(), name='appointment_new'),
	path('appointment_calendar/', appointment_calendar, name='appointment_calendar'),
	path('select_doctor/', select_doctor, name='select_doctor'),
	path('', AppointmentListView.as_view(), name='appointment_list'),
]