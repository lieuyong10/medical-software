# Generated by Django 2.1.5 on 2020-05-03 10:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patients', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='patient',
            name='passport',
            field=models.CharField(default=12, max_length=255),
            preserve_default=False,
        ),
    ]
