from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.urls import reverse_lazy

from .models import Patient

# Create your views here.
class PatientListView(LoginRequiredMixin, ListView):
	model = Patient
	template_name = 'patient_list.html'
	login_url = 'login'

class PatientDetailView(LoginRequiredMixin, DetailView):
	model = Patient
	template_name = 'patient_detail.html'
	login_url = 'login'

class PatientUpdateView(LoginRequiredMixin, UpdateView):
	model = Patient
	fields = ('first_name', 'last_name', 'gender', 'date_of_birth', 'nric', 'passport', 'nationality', 'ethnic_race', 'mobile', 'landline', 'email', 'emergency_contact_name', 'emergency_phone_number', 'emergency_relationship', 'insurance_company', 'insurance_policy_no', 'residential_address_street', 'residential_address_block', 'residential_address_unit', 'residential_address_postal_code', 'residential_address_city', 'residential_address_country', 'shipping_address_same', 'shipping_address_street', 'shipping_address_block', 'shipping_address_unit', 'shipping_address_postal_code', 'shipping_address_city', 'shipping_address_country', 'billing_address_same', 'billing_address_street', 'billing_address_block', 'billing_address_unit', 'billing_address_postal_code', 'billing_address_city', 'billing_address_country', 'doctor')
	template_name = 'patient_edit.html'
	login_url = 'login'
	
class PatientDeleteView(LoginRequiredMixin, DeleteView):
	model = Patient
	template_name = 'patient_delete.html'
	success_url = reverse_lazy('patient_list')
	login_url = 'login'

class PatientCreateView(LoginRequiredMixin, CreateView):
	model = Patient
	template_name = 'patient_new.html'
	fields = ('first_name', 'last_name', 'gender', 'date_of_birth', 'nric', 'passport', 'nationality', 'ethnic_race', 'mobile', 'landline', 'email', 'emergency_contact_name', 'emergency_phone_number', 'emergency_relationship', 'insurance_company', 'insurance_policy_no', 'residential_address_street', 'residential_address_block', 'residential_address_unit', 'residential_address_postal_code', 'residential_address_city', 'residential_address_country', 'shipping_address_same', 'shipping_address_street', 'shipping_address_block', 'shipping_address_unit', 'shipping_address_postal_code', 'shipping_address_city', 'shipping_address_country', 'billing_address_same', 'billing_address_street', 'billing_address_block', 'billing_address_unit', 'billing_address_postal_code', 'billing_address_city', 'billing_address_country', 'doctor')
	login_url = 'login'
	
	def form_valid(self, form): # new
		form.instance.author = self.request.user
		return super().form_valid(form)
	

