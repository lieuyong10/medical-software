from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.urls import reverse
from datetime import date

# Create your models here.

GENDER_CHOICES = (
	('male', 'MALE'),
	('female', 'FEMALE'),
)

MARITAL_STATUS_CHOICES = (
	('single', 'SINGLE'),
	('married', 'MARRIED'),
)

class Patient(models.Model):
	# general data
	first_name = models.CharField(max_length=255)
	last_name = models.CharField(max_length=255)
	gender = models.CharField(max_length=10, blank=True, choices=GENDER_CHOICES)
	date_of_birth = models.DateField('Date', null=True)
	marital_status = models.CharField(max_length=10, blank=True, choices=MARITAL_STATUS_CHOICES)
	nric = models.CharField(max_length=255, blank=True)
	passport = models.CharField(max_length=255, blank=True)
	nationality = models.CharField(max_length=100, blank=True)
	ethnic_race = models.CharField(max_length=100, blank=True)
	
	# contact
	mobile = models.CharField(max_length=20, blank=True)
	landline = models.CharField(max_length=20, blank=True)
	email = models.CharField(max_length=100, blank=True)
	
	# emergency contact
	emergency_contact_name = models.CharField(max_length=100, blank=True)
	emergency_phone_number = models.CharField(max_length=20, blank=True)
	emergency_relationship = models.CharField(max_length=50, blank=True)
	
	# insurance
	insurance_company = models.CharField(max_length=100, blank=True)
	insurance_policy_no = models.CharField(max_length=30, blank=True)
	
	# residential address
	residential_address_street = models.CharField(max_length=50, blank=True)
	residential_address_block = models.CharField(max_length=50, blank=True)
	residential_address_unit = models.CharField(max_length=20, blank=True)
	residential_address_postal_code = models.CharField(max_length=10, blank=True)
	residential_address_city = models.CharField(max_length=30, blank=True)
	residential_address_country = models.CharField(max_length=30, blank=True)
	
	# shipping address
	shipping_address_same = models.CharField(max_length=1, blank=True)
	shipping_address_street = models.CharField(max_length=50, blank=True)
	shipping_address_block = models.CharField(max_length=50, blank=True)
	shipping_address_unit = models.CharField(max_length=20, blank=True)
	shipping_address_postal_code = models.CharField(max_length=10, blank=True)
	shipping_address_city = models.CharField(max_length=30, blank=True)
	shipping_address_country = models.CharField(max_length=30, blank=True)
	
	# billing address
	billing_address_same = models.CharField(max_length=1, blank=True)
	billing_address_street = models.CharField(max_length=50, blank=True)
	billing_address_block = models.CharField(max_length=50, blank=True)
	billing_address_unit = models.CharField(max_length=20, blank=True)
	billing_address_postal_code = models.CharField(max_length=10, blank=True)
	billing_address_city = models.CharField(max_length=30, blank=True)
	billing_address_country = models.CharField(max_length=30, blank=True)
	
	doctor = models.ForeignKey(
		get_user_model(),
		on_delete=models.CASCADE,
	)
		
	def __str__(self):
		return self.first_name
		
	def get_absolute_url(self):
		return reverse('patient_detail', args=[str(self.id)])
		